import Tkinter 
from Tkinter import *
import random
import time

def start_game(event):
    # When game start, remove starting text
    canvas.delete("starting_text")     
    # Constantly telling tkinter to redraw 
    while 1:
        if ball.hit_bottom == False:
            ball.draw()
            paddle.draw()
        else:
            canvas.create_text(250, 200, font="Helvetica", text="Game Over")
        tk.update_idletasks()
        tk.update()
        time.sleep(0.01) 

# Class Ball
class Ball:
    def __init__(self, canvas, paddle, color):
        # Assign value to canvas
        self.canvas = canvas
        self.paddle = paddle
        self.id = canvas.create_oval(10, 10, 25, 25, fill=color)
        # Move the ball to the middle of the canvas
        self.canvas.move(self.id, 245, 100)  

        # Random list
        starts = [-3, -2, -1, 1, 2, 3]
        random.shuffle(starts)
        self.x = starts[0]
        # Top of the ball
        self.y = -3
        self.canvas_height = self.canvas.winfo_height()
        self.canvas_width  = self.canvas.winfo_width()
        self.hit_bottom = False

    def draw(self):
        self.canvas.move(self.id, self.x, self.y)
        # Get the current x and y coordinates of the ball
        # coords returns [x1, y1, x2, y2]
        pos = self.canvas.coords(self.id)

        # If hit the top, stop moving up
        if pos[1] <= 0:
            self.y = 1

        # If hit the bottom, stop moving down
        if pos[3] >= self.canvas_height:
            self.hit_bottom = True

        if self.hit_paddle(pos) == True:
            self.y = -6
        
        # If hit the left wall, bounce
        if pos[0] <= 0:
            self.x = 3
        
        # If hit the right wall, bounce
        if pos[2] >= self.canvas_width:
            self.x = -3

    def hit_paddle(self, pos):
        paddle_pos = self.canvas.coords(self.paddle.id)
        if pos[2] >= paddle_pos[0] and pos[0] <= paddle_pos[2]:
            if pos[3] >= paddle_pos[1] and pos[3] <= paddle_pos[3]:
                return True
        return False

class Paddle:
    def __init__(self, canvas, color):
        self.canvas = canvas
        self.id = canvas.create_rectangle(0, 0, 100, 10, fill=color)
        self.canvas.move(self.id, 200, 300)
        self.x = 0
        self.canvas_width = self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)

    def draw(self):
        self.canvas.move(self.id, self.x, 0)
        pos = self.canvas.coords(self.id)
        if pos[0] <= 0:
            self.x = 0
        if pos[2] >= self.canvas_width:
            self.x = 0

    def turn_left(self, evt):
        self.x = -2

    def turn_right(self, evt):
        self.x = 2

tk = Tkinter.Tk()
tk.title("Game")              # Set window title
tk.resizable(0, 0)           # Window size is fixed
tk.wm_attributes("-topmost", 1) # place window in front of all other windows 
canvas = Canvas(tk, width=500, height=400, bd=0, highlightthickness=0)
canvas.create_text(250, 200, font="Helvetica", text="Left click to Start", tag="starting_text")
canvas.pack()                 # Tells canvas to size itself 
tk.update()                   # Tells tkinter to initialize itself


# Create Paddle 
paddle = Paddle(canvas, 'blue')
# Create a Ball object
ball = Ball(canvas, paddle, 'red')
# Bind left click to function start_game      
canvas.bind("<Button-1>", start_game)
tk.mainloop()

